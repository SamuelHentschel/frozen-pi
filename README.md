# SETUP

1. make the binary

    * ```make```

2. clear SD card (you only need to do about 10%)

    * ```shred -vz /dev/sdb```

3. set SD card partition table

    * ```sudo cfdisk /dev/sdb```

    * select dos partition scheme

    * create one partition with new (full size)

    * make bootable

    * select W95 FAT32 (LBA) partition type

    * write the information

4. mount the SD card (/dev/sdb1 for me)

    * ```sudo mount /dev/sdXY /mnt```

5. make filesystem type

    *  ```mkfs.fat -F 32 /dev/sdb1```

6. copy the boot files and binary to the SD card

    * ```sudo cp boot/start.elf boot/bootcode.bin boot/config.txt uart01.bin /mnt/```

7. unmount SD card

    * ```sudo umount -R /mnt/```

8. plug into PI and connect FTDI cable

9. attach to uart terminal (on mine its /dev/ttyUSB0)

    * ```screen /dev/ttyUSB0 115200```

10. attach FTDI to USB port of computer

11. profit
